import { LitElement, html, customElement } from 'lit-element';
import { openWcLogo } from '../open-wc-logo.js';
import '../prestamo-form/prestamo-form.js'
import '../muestra-huella/muestra-huella.js'

class BasePrestamos extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
      prestamo: { type: Object },
    };
  }

  constructor(){
    super();

    this.prestamo = {};
    this.bgi = "../img/0-6.jpg";

    let xhr = new XMLHttpRequest ();
    xhr.onload = () => {
        if (xhr.status === 200) {
            console.log ("Petición tokenget recibida");
            let APIResponse = xhr.responseText;
            console.log(APIResponse);
            this.token = APIResponse;
        }
    }
    xhr.open ("GET", "http://localhost:8081/apihackaton/v2/tokenget?nombre=test2");
    console.log (this.huella);
    xhr.send(JSON.stringify(this.huella));
    
  }

  render() {
    
;    return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        
        <h1>Préstamos sostenibles</h1>

        <div>
        <div class="alert alert-success d-none" role="alert" id="success-message">
          El prestamo solicitado ha sido concedido!
        </div>

        <div class="alert alert-warning d-none" role="alert" id="pendiente-message">
          El prestamo está pendiente...
        </div>

        <div class="alert alert-danger d-none" role="alert" id="alert-message">
          El prestamo solicitado ha sido rechazado!
        </div>
        </div>

        <div>
          <muestra-huella id="muestraHuella" @change-bg-image="${this.changeBGImage}"></muestra-huella>
          <div id="maindiv" style="background-image: url('${this.bgi}');">
            <prestamo-form id="prestamoForm" class="border rounded border-rounded" @calcular-huella="${this.calcularHuella}" @solicitar-prestamo-form="${this.solicitarPrestamo}">
            </prestamo-form>
            </div>
         </div>
    `;
  }

  //metodos privados
  changeBGImage (e) {
    console.log("changeBGImage en BasePrestamos");
    let score = e.detail.scoring;
    console.log(score);
    if (score < 7) {
      console.log("changeBGImage 1");
      this.bgi = "../img/0-6.jpg";
    } else if (score < 11) {
      console.log("changeBGImage 2");
      this.bgi = "../img/7-10.jpg";
    } else if (score < 99) {
      console.log("changeBGImage 3");
      this.bgi = "../img/11-99.jpg";
    } else {
      console.log("changeBGImage 4");
      this.bgi = "../img/0-6.jpg";
    }
    console.log("background-image: url('"+this.bgi+"');");
    //this.shadowRoot.getElementById("maindiv").style["background-image"] = "url('"+this.bgi+"');";
    this.shadowRoot.getElementById('maindiv').style.backgroundImage="url("+this.bgi+")"; 
  }
  calcularHuella (e) {
    console.log("calcularHuella en BasePrestamos");
    this.shadowRoot.getElementById("muestraHuella").huella = e.detail.huella;
    this.shadowRoot.getElementById("muestraHuella").actualizar = !this.shadowRoot.getElementById("muestraHuella").actualizar;

  }

  solicitarPrestamo(e){
    //ocultamos el mensaje de éxito
    this.hideSuccessMessage();
    this.hideWarningMessage();
    this.hidePendienteMessage();
    
    console.log("Solicitar prestamo en BasePrestamos");
    console.log(e.detail.prestamo);
    this.prestamo = e.detail.prestamo;
    this.prestamo.estado = "nuevo";

    //llamada al servicio de guardar el prestamo
    let xhr = new XMLHttpRequest ();
    xhr.onload = () => {
        if (xhr.status === 201) {
              console.log ("Petición POST crear prestamo");
              let APIResponse = JSON.parse(xhr.responseText);
              this.prestamo = APIResponse;

              if (this.prestamo){
              console.log("estado del prestamo: " + this.prestamo.estado);

              if (this.prestamo.estado === 'concedido'){
                console.log("prestamo concedido");
                
                this.showSuccessMessage();
              }
              else if (this.prestamo.estado === 'pendiente') {
                console.log("prestamo pendiente");
                this.showPendienteMessage();
              }
              else if (this.prestamo.estado === 'rechazado'){
                this.showWarningMessage();
              }
            }
        }
    }
    xhr.open ("POST", "http://localhost:8081/apihackaton/v2/prestamos");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.setRequestHeader("Authorization", this.token);
    
    xhr.send(JSON.stringify(this.prestamo));
  }

  showSuccessMessage(){
    console.log("show success message");
    this.shadowRoot.getElementById("success-message").classList.remove("d-none");
    this.shadowRoot.getElementById("success-message").classList.add("d-inline");

  }

  hideSuccessMessage(){
    console.log("hide success message");
    this.shadowRoot.getElementById("success-message").classList.remove("d-inline");
    this.shadowRoot.getElementById("success-message").classList.add("d-none");
  }

  showPendienteMessage(){
    console.log("show pendiente message");
    this.shadowRoot.getElementById("pendiente-message").classList.remove("d-none");
    this.shadowRoot.getElementById("pendiente-message").classList.add("d-inline");

  }

  hidePendienteMessage(){
    console.log("hide pendiente message");
    this.shadowRoot.getElementById("pendiente-message").classList.remove("d-inline");
    this.shadowRoot.getElementById("pendiente-message").classList.add("d-none");
  }

  showWarningMessage(){
    console.log("show warning message");
    this.shadowRoot.getElementById("alert-message").classList.remove("d-none");
    this.shadowRoot.getElementById("alert-message").classList.add("d-inline");
  }

  hideWarningMessage(){
    console.log("hide warning message");
    this.shadowRoot.getElementById("alert-message").classList.remove("d-inline");
    this.shadowRoot.getElementById("alert-message").classList.add("d-none");
  }
}

customElements.define('base-prestamos', BasePrestamos);
