import { LitElement, html, customElement } from 'lit-element';

class PrestamoForm extends LitElement {
    static get properties(){
        return {
            prestamo: {type: Object}
        };
    }

    constructor(){
        super();
        console.log("constructor prestamo form");
        
        this.resetFormData();
    }

    render()  {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       
        <div>
            <form>
                <div class="row">
                    <div class="col-6 form-group">
                        <h3>Datos personales</h3>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input required .value="${this.prestamo.nombre}" @input="${this.actualizarNombre}" type="text" id="formGivenName" 
                            class="form-control" placeholder="Nombre" />
                        </div>
                        <div class="form-group">
                            <label>Apelllido</label>
                            <input .value="${this.prestamo.apellido1}" @input="${this.actualizarApellidos}" type="text" id="formFamilyName1" class="form-control" placeholder="Apellido 1" />
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input .value="${this.prestamo.dni}" @input="${this.actualizarDni}" type="text" id="formDNI" class="form-control" placeholder="DNI" />
                        </div>
                    </div>
                    <div class="col-6 form-group">
                        <h3>Datos huella y préstamo</h3>
                        <div class="form-group">
                            <label>Importe</label>
                            <input .value="${this.prestamo.capital}"  @input="${this.actualizarImporte}" type="number" id="formImporte" class="form-control" placeholder="Importe" />
                        </div>
                        <div class="form-group">
                            <label>Vehículo</label>
                            <select class="custom-select" id="vehiculoSel" @change="${this.cambiarVehiculo}">
                                <option value="" selected>Vehículo</option>
                                <option value="no">No tengo</option>
                                <option value="electrico">Eléctrico</option>
                                <option value="gasolina">Gasolina</option>
                                <option value="diesel">Diesel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Calefacción</label>
                            <select class="custom-select" id="calefaccionSel" @change="${this.cambiarCalefaccion}">
                                <option value="" disabled selected>Calefacción</option>
                                <option value="no">No tengo (wat?)</option>
                                <option value="gas">Gas Natural</option>
                                <option value="butano">Butano</option>
                                <option value="carbon">Carbón</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Dieta</label>
                            <select class="custom-select" id="dietaSel" @change="${this.cambiarDieta}">
                                <option value="" disabled selected>Dieta</option>
                                <option value="vegana">Vegana</option>
                                <option value="vegetariana">Vegetariana</option>
                                <option value="mediterranea">Mediterranea</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Plazo</label>
                            <select class="custom-select" id="plazoSel" @change="${this.cambiarPlazo}">
                                <option value="" disabled selected>Plazo</option>
                                <option value="3">3 meses</option>
                                <option value="6">6 meses</option>
                                <option value="12">12 meses</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Consumo electricidad (kwh/año)</label>
                            <input .value="${this.prestamo.huella.electricidad}" @input="${this.actualizarElectricidad}" type="number" id="formElectricidad" class="form-control" placeholder="Importe" />
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" @click=${this.calculateFingerprint}><strong>Calcular huella</strong></button>
                <button class="btn btn-success" @click=${this.enviarPrestamo}><strong>Solicitar Prestamo</strong></button>
            </form>
        </div>
        `;
    }

    actualizarNombre(e){
        console.log("actualizarNombre con el valor "  + e.target.value);
        this.prestamo.nombre = e.target.value;
    }

    actualizarApellidos(e){
        console.log("actualizarApellidos con el valor "  + e.target.value);
        this.prestamo.apellido1 = e.target.value;
    }

    actualizarDni(e){
        console.log("actualizarDni con el valor "  + e.target.value);
        this.prestamo.dni = e.target.value;
    }

    actualizarImporte(e){
        console.log("actualizarImporte con el valor "  + e.target.value);
        this.prestamo.importe = e.target.value;
    }

    actualizarElectricidad(e){
        console.log("actualizarElectricidad con el valor "  + e.target.value);
        this.prestamo.huella.electricidad = e.target.value;
    }

    cambiarVehiculo(e){
        console.log("cambiar vehiculo");
        this.prestamo.huella.vehiculo = this.shadowRoot.querySelector('#vehiculoSel').value;
        console.log("Ha cambiado el vehiculo de la huella al valor: " + this.prestamo.huella.vehiculo);
    }
    
    cambiarCalefaccion(e){
        console.log("cambiar calefacción");
        this.prestamo.huella.calefaccion = this.shadowRoot.querySelector('#calefaccionSel').value;
        console.log("Ha cambiado la calefacción de la huella al valor: " + this.prestamo.huella.calefaccion);
    }

    cambiarDieta(e){
        console.log("cambiar dieta");
        this.prestamo.huella.dieta = this.shadowRoot.querySelector('#dietaSel').value;
        console.log("Ha cambiado la dieta de la huella al valor: " + this.prestamo.huella.dieta);
    }

    cambiarPlazo(e){
        console.log("cambiar plazo");
        this.prestamo.plazo = this.shadowRoot.querySelector('#plazoSel').value;
        console.log("Ha cambiado el plazo del prestamo al valor: " + this.prestamo.plazo);
    }

    enviarPrestamo(e){
        e.preventDefault();

        console.log("enviar prestamo");
        console.log(this.prestamo);

        //evento pedir prestamo
        this.dispatchEvent(new CustomEvent("solicitar-prestamo-form",  {
            detail: {
                prestamo : this.prestamo,
                editingPerson: this.editingPerson
            }
        }))

    }

    resetFormData(){
        console.log("reset form data");

        this.prestamo = {};
        this.prestamo.nombre = "";
        this.prestamo.apellido1 = "";
        this.prestamo.dni = "";
        this.prestamo.capital = "";

        this.prestamo.huella = {};
        this.prestamo.huella.vehiculo = "";
        this.prestamo.huella.calefaccion = "";
        this.prestamo.huella.electricidad = "";
        this.prestamo.huella.dieta = "";

        this.editingPerson = false;
    }

    calculateFingerprint (e) {
        e.preventDefault();

        console.log("calculateFingerprint");
        console.log(this.prestamo.huella);

        this.dispatchEvent(new CustomEvent("calcular-huella",  {
            detail: {
                huella : this.prestamo.huella,
            }
        }))
        return true;

        // aquí tenemos que llamar por AJAX para actualizar la huella de carbono...
        let xhr = new XMLHttpRequest ();
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log ("Petición get huella recibida");
                let APIResponse = JSON.parse(xhr.responseText);
                this.movies = APIResponse.results;
            }
        }
        xhr.open ("POST", "http://localhost:8081/apihackaton/v1/util/calculohuella");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(this.prestamo.huella));
        console.log (this.prestamo.huella);
    }
}

customElements.define('prestamo-form', PrestamoForm);