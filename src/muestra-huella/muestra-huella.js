import {LitElement, html} from 'lit-element';

class MuestraHuella extends LitElement {
    static get properties() {
        return {
            huella: { type: Object },
            scoreHuella : { type: Number },
            actualizar : {type : Boolean},
            textoHuella: {type : String}
        };
    }
    constructor(){
        super();
        this.scoreHuella = -1;
        this.huella = {};
        this.actualizar = false;
        /*
        this.huella.vehiculo = "";
        this.huella.calefaccion = "";
        this.huella.electricidad = "";
        this.huella.dieta = "";
        */
        this.cambiaTextoHuella ();
    }
    updated (changedProperties) {
        console.log ("changedProperties");
        if (changedProperties.has("actualizar")) {
            if (Object.keys(this.huella).length != 0) {
                this.obtieneScoring ();
            }
        }
    }

    render () {
        return html`
            <div>${this.textoHuella}</div>
        `;
    }
    cambiaTextoHuella () {
        if (this.scoreHuella == -1) {
            this.textoHuella = "Pulsa 'calcular huella' para calcular tu huella";
        } else {
            this.textoHuella =  "Tu huella es " + this.scoreHuella;
            this.dispatchEvent(new CustomEvent("change-bg-image",  {
                detail: {
                    scoring : this.scoreHuella
                }
            }))
        }
    }
    obtieneScoring () {
        // aquí tenemos que llamar por AJAX para actualizar la huella de carbono...
        let xhr = new XMLHttpRequest ();
        xhr.onload = () => {
            if (xhr.status === 202 || xhr.status === 200) {
                console.log ("Petición get huella recibida");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.scoreHuella = APIResponse;
                this.cambiaTextoHuella ();
            }
        }
        xhr.open ("POST", "http://localhost:8081/apihackaton/v1/util/calculohuella");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        console.log (this.huella);
        xhr.send(JSON.stringify(this.huella));
    }
}

customElements.define('muestra-huella' , MuestraHuella);